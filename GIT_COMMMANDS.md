# Git Command line instructions

###### Git global setup

`git config --global user.name "user_name"` <br />
`git config --global user.email "user_email"` <br />

------------------------

###### Create a new repository

`git clone https://gitlab.com/java_learning_pt/practice1.git` <br />
`cd Practice1` <br />
`git switch -c main` <br />
`touch README.md` <br />
`git add README.md` <br />
`git commit -m "add README"` <br />
`git push -u origin main` <br />

------------------------

###### Push an existing folder

`cd existing_folder` <br />
`git init --initial-branch=master` <br />
`git remote add origin https://gitlab.com/java_learning_pt/practice1.git` <br />
`git add .` <br />
`git commit -m "Initial commit"` <br />
`git push -u origin master` <br />

------------------------

###### Push an existing Git repository

`cd existing_repo` <br />
`git remote rename origin old-origin` <br />
`git remote add origin https://gitlab.com/java_learning_pt/practice1.git` <br />
`git push -u origin --all` <br />
`git push -u origin --tags` <br />